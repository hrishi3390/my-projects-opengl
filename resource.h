//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DemoSolarSystem.rc
//
#define IDB_BITMAP1                     101
#define IDB_BITMAP2                     102
#define IDB_BITMAPj2                    102
#define IDB_BITMAP3                     103
#define IDB_BITMAP4                     104
#define IDB_BITMAP5                     105
#define IDB_BITMAP6                     106
#define IDB_BITMAP7                     107
#define IDB_BITMAP8                     108
#define IDB_BITMAP9                     109
#define IDB_BITMAP10                    110
#define IDB_BITMAP11                    111
#define IDR_WAVE1                       112
#define IDB_BITMAP12                    113
#define MERCURY_BITMAP                  201
#define VENUS_BITMAP                    202
#define EARTH_BITMAP                    203
#define MARS_BITMAP                     204
#define JUPITER_BITMAP                  205
#define SATURN_BITMAP                   206
#define URANUS_BITMAP                   207
#define NEPTUNE_BITMAP                  208
#define MOON_BITMAP                     209
#define SUN_BITMAP                      210

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        114
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
