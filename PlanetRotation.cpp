
#include "PlanetRotation.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>

enum Plantes {SUN, MERCURY, VENUS, EARTH, MARS, JUPITAR, SATURN, URANUS, NEPTUNE, PLUTO};

GLfloat earthDay = 0.0f;
GLfloat earthYear = 260.0f;
GLfloat moonYear = 40.0f;
GLfloat moonDay = 0.0f;
GLfloat mercuryYear = 44.0f;
GLfloat mercuryDay = 0.0f;
GLfloat venusYear = 100.0f;
GLfloat venusDay = 0.0f;
GLfloat marsYear = 450.0f;
GLfloat marsDay = 0.0f;
GLfloat jupiterYear = 1800.0f;
GLfloat jupiterDay = 0.0f;
GLfloat saturnYear = 8600.0f;
GLfloat saturnDay = 0.0f;
GLfloat uranusYear = 180.0f;
GLfloat uranusDay = 0.0f;
GLfloat neptuneYear = 40210.0f;
GLfloat neptuneDay = 0.0f;
GLfloat plutoYear = 270.0f;
GLfloat plutoDay = 0.0f;

void updateRotation(const GLfloat&, const GLfloat&, GLfloat&, GLfloat&, GLfloat&, GLfloat&);
void DrawSaturnRing(const GLfloat &planetRadius, const GLfloat &planetOrbitRadius, GLfloat &planetOrbitDuration, GLfloat &planetRotation, const GLuint &planet_texture);

GLUquadric *quadric = NULL;
bool isFirstRun = true;
//GLfloat orbitFactor = 0.0035f;//0.01
//GLfloat selfRoationFactor = 0.0035f;

GLfloat i = mercuryYear;
GLfloat j = 0.0f;
GLfloat k = venusYear;
GLfloat l = 0.0f;
GLfloat m = earthYear;
GLfloat n = 0.0f;
GLfloat o = marsYear;
GLfloat p = 0.0f;
GLfloat q = jupiterYear;
GLfloat r = 0.0f;
GLfloat s = saturnYear;
GLfloat t = 0.0f;
GLfloat u = uranusYear;
GLfloat v = 0.0f;
GLfloat w = neptuneYear;
GLfloat x = 0.0f;
GLfloat y = moonYear;
GLfloat z = 0.0f;


void updateRotation(const GLfloat &PLANET_DAY, const GLfloat &PLANET_YEAR, GLfloat &dayCounter, GLfloat &yearCounter, GLfloat &rotationInDay, GLfloat &rotationInYear)
{

	rotationInYear = 360 * (yearCounter) / PLANET_YEAR;
	yearCounter += orbitFactor;

	if (yearCounter >= PLANET_YEAR)
		yearCounter = 0;


	rotationInDay = 360 * (dayCounter) / PLANET_DAY;
	dayCounter += selfRoationFactor;

	if (dayCounter >= PLANET_DAY)
		dayCounter = 0;

}


void updatePlanetRotation()
{
	updateRotation(MERCURY_DAY, MERCURY_YEAR, j, i, mercuryDay, mercuryYear);
	updateRotation(VENUS_DAY, VENUS_YEAR, k, l, venusDay, venusYear);
	updateRotation(EARTH_DAY, EARTH_YEAR, n, m, earthDay, earthYear);
	updateRotation(MOON_DAY, MOON_YEAR, z, y, moonDay, moonYear);
	updateRotation(MARS_DAY, MARS_YEAR, p, o, marsDay, marsYear);
	updateRotation(JUPITER_DAY, JUPITER_YEAR, r, q, jupiterDay, jupiterYear);
	updateRotation(SATURN_DAY, SATURN_YEAR, t, s, saturnDay, saturnYear);
	updateRotation(URANUS_DAY, URANUS_YEAR, v, u, uranusDay, uranusYear);
	updateRotation(NEPTUNE_DAY, NEPTUNE_YEAR, x, w, neptuneDay, neptuneYear);
}



void DrawPlanetRevolutionPath()
{
	int count = 8;
	int color = 0;
	static int totalCordinates = 0;
	GLfloat fFactor = 0.1f;
	GLfloat fRadius = 2.0f;
	static std::vector<float> XcircleCoordinates;
	static std::vector<float> YcircleCoordinates;

	if (isFirstRun)
	{
		for (GLfloat i = 0; i <= 360; i = i + 1)
		{
			GLfloat X_cordinate = (GLfloat)cos(i * (M_PI / 180));
			XcircleCoordinates.push_back(X_cordinate);

			GLfloat Y_cordinate = (GLfloat)sin(i * (M_PI / 180));
			YcircleCoordinates.push_back(Y_cordinate);
			totalCordinates++;
		}
		isFirstRun = false;
	}
	

	do {
		glBegin(GL_POINTS);
		glColor3f(1.0f, 1.0f, 1.0f);

		for(std::vector<float>::iterator x = XcircleCoordinates.begin(), y = YcircleCoordinates.begin();x!= XcircleCoordinates.end(), y!= YcircleCoordinates.end(); ++x, ++y)
		{
			GLfloat r = (GLfloat)(fRadius)* (*x);
			GLfloat s = (GLfloat)(fRadius) * (*y);
			glVertex3f(r, s , 0.0f);
		}
		fRadius = fRadius + 1;
		count--;
		glEnd();
	} while (count);
}

void DrawPlanet(const GLfloat &planetRadius, const GLfloat &planetOrbitRadius,  GLfloat &planetOrbitDuration,  GLfloat &planetRotation, const GLuint &planet_texture ) 
{
	glRotatef((GLfloat)planetOrbitDuration, 0.0f, 1.0f, 0.0f);
	glTranslatef(planetOrbitRadius, 0.0f, 0.0f); //bhovtina phiravta if rotation chya nanter cha translation
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)planetRotation, 0.0f, 0.0f, 1.0f); //later axis will be changed // translate karun rotate mhanje swatha bhovati phirne
	quadric = gluNewQuadric();
	gluSphere(quadric, planetRadius, 20, 20);
	gluQuadricDrawStyle(quadric, GLU_FILL);
	glBindTexture(GL_TEXTURE_2D, planet_texture);
	gluQuadricTexture(quadric, true);
	gluSphere(quadric, planetRadius, 20, 20);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DrawSun()
{
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	quadric = gluNewQuadric();
	gluQuadricDrawStyle(quadric, GLU_FILL);
	glBindTexture(GL_TEXTURE_2D, sun_texture);
	gluQuadricTexture(quadric, true);
	gluSphere(quadric, SUN_RADIUS, 30, 30);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DrawSolarSystem()
{
	
	DrawSun();
	//*******************************************************************************//
	glPopMatrix();
	glPushMatrix();

	DrawPlanet(MERCURY_RADIUS, MERCURY_ORBIT_RADIUS, mercuryYear, mercuryDay, mercury_texture);
	//*******************************************************************************//
	glPopMatrix();
	glPushMatrix();

	DrawPlanet(VENUS_RADIUS, VENUS_ORBIT_RADIUS, venusYear, venusDay, venus_texture);

	//*******************************************************************************//
	glPopMatrix();
	glPushMatrix();

	//****************************************************Draw Earth***************************************************
	glRotatef((GLfloat)earthYear, 0.0f, 1.0f, 0.0f);
	glTranslatef(4.0f, 0.0f, 0.0f); //bhovtina phiravta if rotation chya nanter cha translation

	glPushMatrix();

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)earthDay, 0.0f, 0.0f, 1.0f); //later axis will be changed // translate karun rotate mhanje swatha bhovati phirne
	quadric = gluNewQuadric();
	gluQuadricDrawStyle(quadric, GLU_FILL);
	glBindTexture(GL_TEXTURE_2D, earth_texture);
	gluQuadricTexture(quadric, true);
	gluSphere(quadric, EARTH_RADIUS, 20, 20);
	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();

	glRotatef((GLfloat)moonYear, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.5f, 0.0f, 0.0f); //bhovtina phiravta if rotation chya nanter cha translation
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)moonDay, 0.0f, 0.0f, 1.0f); //later axis will be changed // translate karun rotate mhanje swatha bhovati phirne

	quadric = gluNewQuadric();
	gluQuadricDrawStyle(quadric, GLU_FILL);
	glBindTexture(GL_TEXTURE_2D, moon_texture);
	gluQuadricTexture(quadric, true);
	gluSphere(quadric, MOON_RADIUS, 20, 20);
	glBindTexture(GL_TEXTURE_2D, 0);
	//***********************************************************************************************//
	glPopMatrix();

	glPopMatrix();
	glPushMatrix();

	DrawPlanet(MARS_RADIUS, MARS_ORBIT_RADIUS, marsYear, marsDay, mars_texture);
	//***********************************************************************************************//
	glPopMatrix();
	glPushMatrix();


	DrawPlanet(JUPITER_RADIUS, JUPITER_ORBIT_RADIUS, jupiterYear, jupiterDay, jupiter_texture);
	//***********************************************************************************************//
	glPopMatrix();
	glPushMatrix();

	DrawPlanet(SATURN_RADIUS, SATURN_ORBIT_RADIUS, saturnYear, saturnDay, saturn_texture);
	glPopMatrix();
	glPushMatrix();

	DrawSaturnRing(SATURN_RADIUS, SATURN_ORBIT_RADIUS, saturnYear, saturnDay, saturn_ring_texture);

	//***********************************************************************************************//
	glPopMatrix();
	glPushMatrix();

	DrawPlanet(URANUS_RADIUS, URANUS_ORBIT_RADIUS, uranusYear, uranusDay, uranus_texture);
	//***********************************************************************************************//
	glPopMatrix();
	glPushMatrix();


	DrawPlanet(NEPTUNE_RADIUS, NEPTUNE_ORBIT_RADIUS, neptuneYear, neptuneDay, neptune_texture);
	//***********************************************************************************************//

}

void DrawSaturnRing(const GLfloat &planetRadius, const GLfloat &planetOrbitRadius, GLfloat &planetOrbitDuration, GLfloat &planetRotation, const GLuint &planet_texture)
{
	glRotatef((GLfloat)planetOrbitDuration, 0.0f, 1.0f, 0.0f);
	glTranslatef(planetOrbitRadius, 0.0f, 0.0f); //bhovtina phiravta if rotation chya nanter cha translation
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)planetRotation, 0.0f, 0.0f, 1.0f); //later axis will be changed // translate karun rotate mhanje swatha bhovati phirne
	quadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, planet_texture);
	gluQuadricTexture(quadric, true);
	glScalef(1, 1, .02f);
	gluSphere(quadric, planetRadius*2, 20, 20);
	glBindTexture(GL_TEXTURE_2D, 0);
}