#pragma once
#include<Windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/glu.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include "resource.h"




extern GLUquadric *quadric;

/*
extern GLfloat earthDay;
extern GLfloat earthYear;
extern GLfloat moonYear;
extern GLfloat moonDay;
extern GLfloat mercuryYear;
extern GLfloat mercuryDay;
extern GLfloat venusYear;
extern GLfloat venusDay;
extern GLfloat marsYear;
extern GLfloat marsDay;
extern GLfloat jupiterYear;
extern GLfloat jupiterDay;
extern GLfloat saturnYear;
extern GLfloat saturnDay;
extern GLfloat uranusYear;
extern GLfloat uranusDay;
extern GLfloat neptuneYear;
extern GLfloat neptuneDay;
extern GLfloat plutoYear;
extern GLfloat plutoDay;
*/



//texture variable
extern GLuint mercury_texture;
extern GLuint venus_texture;
extern GLuint earth_texture;
extern GLuint mars_texture;
extern GLuint jupiter_texture;
extern GLuint saturn_texture;
extern GLuint uranus_texture;
extern GLuint neptune_texture;
extern GLuint moon_texture;
extern GLuint sun_texture;
extern GLuint saturn_ring_texture;

extern bool isYZRotationDone;
extern bool isSunToOuterDone;

extern GLfloat fXYRotationFactor;
extern GLfloat orbitFactor;
extern GLfloat selfRoationFactor;




void LoadAndEnableTexture();
void LookAtFromSunToOuter();
void LookAtFromOuterToSun();
void LookAtYZRotation();