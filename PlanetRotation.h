#pragma once
#include<Windows.h>
#include<gl/GL.h>
#include<gl/glu.h>
#include <stdio.h>
#include "Global.h"


extern FILE *gpFILE;

enum COLOR { RED = 2, GREEN, BLUE, CYAN, MAGENTA, YELLOW, WHITE, GRAY, ORANGE, MYCOLOR };

const GLfloat MERCURY_RADIUS = 0.1f;
const GLfloat VENUS_RADIUS = 0.18f;
const GLfloat EARTH_RADIUS = 0.26f;
const GLfloat MARS_RADIUS = 0.3f;
const GLfloat JUPITER_RADIUS = 0.6f;
const GLfloat SATURN_RADIUS = 0.5f;
const GLfloat URANUS_RADIUS = 0.3f;
const GLfloat NEPTUNE_RADIUS = 0.3f;
const GLfloat PLUTO_RADIUS = 0.05f;
const GLfloat SUN_RADIUS = 1.0f;
const GLfloat MOON_RADIUS = 0.13f;

const GLfloat MERCURY_ORBIT_RADIUS = 2.0f;
const GLfloat VENUS_ORBIT_RADIUS = 3.0f;
const GLfloat EARTH_ORBIT_RADIUS = 4.0f;
const GLfloat MARS_ORBIT_RADIUS = 5.0f;
const GLfloat JUPITER_ORBIT_RADIUS = 6.0f;
const GLfloat SATURN_ORBIT_RADIUS = 7.0f;
const GLfloat URANUS_ORBIT_RADIUS = 8.0f;
const GLfloat NEPTUNE_ORBIT_RADIUS = 9.0f;



const GLfloat MERCURY_YEAR = 88.0f;
const GLfloat VENUS_YEAR = 225.0f;
const GLfloat EARTH_YEAR = 365.0f;
const GLfloat MARS_YEAR = 686.0f;
const GLfloat JUPITER_YEAR = 4333.0f;
const GLfloat SATURN_YEAR = 10759.0f;
const GLfloat URANUS_YEAR = 30685.0f;
const GLfloat NEPTUNE_YEAR = 60190.0f;
const GLfloat PLUTO_YEAR = 90800.0f;
const GLfloat MOON_YEAR = 1.0f;


const GLfloat MERCURY_DAY = 59.0f;
const GLfloat VENUS_DAY = 243.0f;
const GLfloat EARTH_DAY = 1.0f;
const GLfloat MARS_DAY = 1.04f;
const GLfloat JUPITER_DAY = 0.4f;
const GLfloat SATURN_DAY = 0.4f;
const GLfloat URANUS_DAY = 0.7f;
const GLfloat NEPTUNE_DAY = 0.7f;
const GLfloat PLUTO_DAY = 0.6f;
const GLfloat MOON_DAY = 1.0f;


const GLfloat MERCURY_INCLINATION = 7.0f;
const GLfloat VENUS_INCLINATION = 3.0f;
const GLfloat MARS_INCLINATION = 2.0f;
const GLfloat JUPITER_INCLINATION = 1.0f;
const GLfloat SATURN_INCLINATION = 2.0f;
const GLfloat URANUS_INCLINATION = 1.0f;
const GLfloat NEPTUNE_INCLINATION = 2.0f;
const GLfloat PLUTO_INCLINATION = 1.0f;
const GLfloat EARTH_INCLINATION = 7.0f;


extern  GLfloat earthDay;
extern GLfloat earthYear;
extern GLfloat moonYear;
extern GLfloat moonDay;
extern GLfloat mercuryYear;
extern GLfloat mercuryDay;
extern GLfloat venusYear;
extern GLfloat venusDay;
extern GLfloat marsYear;
extern GLfloat marsDay;
extern GLfloat jupiterYear;
extern GLfloat jupiterDay;
extern GLfloat saturnYear;
extern GLfloat saturnDay ;
extern GLfloat uranusYear;
extern GLfloat uranusDay;
extern GLfloat neptuneYear;
extern GLfloat neptuneDay;



void updatePlanetRotation();
void DrawPlanetRevolutionPath();
void DrawPlanet(const GLfloat &planetRadius, const GLfloat &planetOrbitRadius,  GLfloat &planetOrbitDuration,  GLfloat &planetRotation, const GLuint& planet_texture);
void DrawSolarSystem();