#include "Global.h"


//********************************SPEED FACTORS ********************************
GLfloat orbitFactor = 0.0035f;//0.01
GLfloat selfRoationFactor = 0.0035f;
GLfloat sunToOuterSpeed = 0.004;
GLfloat outerToSunSpeed = 0.01;

//*********************************************************************************

bool drawRevolutionPath = false;


//texture variable
GLuint mercury_texture;
GLuint venus_texture;
GLuint earth_texture;
GLuint mars_texture;
GLuint jupiter_texture;
GLuint saturn_texture;
GLuint uranus_texture;
GLuint neptune_texture;
GLuint moon_texture;
GLuint sun_texture;
GLuint saturn_ring_texture;


bool isYZRotationDone = false;
bool isSunToOuterDone = false;

GLfloat fXYRotationFactor = 0.15f;

bool LoadGLTexture(GLuint *texture, TCHAR resouceID[])
{
	//variable declaration
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resouceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION); //Load Resource create DIB section, device independent bitmat

	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);// get information from OS
												 //actual texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture); //like calloc
												//setting of texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);//MAGNIFICATION
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//following call willactually push data to grahics memorny with help of grahics driver
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);// GL_BGT_TXT changes with image type
																												 //in programmable pipeline 1) glubuild2dmipmaps = glteximage glGeneratemimap
		DeleteObject(hBitmap);
	}

	return bResult;
}

void LoadAndEnableTexture()
{
	LoadGLTexture(&mercury_texture, MAKEINTRESOURCE(MERCURY_BITMAP));
	LoadGLTexture(&venus_texture, MAKEINTRESOURCE(VENUS_BITMAP));
	LoadGLTexture(&earth_texture, MAKEINTRESOURCE(EARTH_BITMAP));
	LoadGLTexture(&mars_texture, MAKEINTRESOURCE(MARS_BITMAP));
	LoadGLTexture(&jupiter_texture, MAKEINTRESOURCE(JUPITER_BITMAP));
	LoadGLTexture(&saturn_texture, MAKEINTRESOURCE(SATURN_BITMAP));
	LoadGLTexture(&uranus_texture, MAKEINTRESOURCE(URANUS_BITMAP));
	LoadGLTexture(&neptune_texture, MAKEINTRESOURCE(NEPTUNE_BITMAP));
	LoadGLTexture(&moon_texture, MAKEINTRESOURCE(MOON_BITMAP));
	LoadGLTexture(&sun_texture, MAKEINTRESOURCE(SUN_BITMAP));
	LoadGLTexture(&saturn_ring_texture, MAKEINTRESOURCE(IDB_BITMAP12));

	glEnable(GL_TEXTURE_2D);

}

void LookAtFromSunToOuter()
{
	static GLfloat zPath = 0.0f;
	gluLookAt(0.0f, 0.0, zPath, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	if (zPath >= 15.0f)
	{
		isSunToOuterDone = true;

	}
	else
	{
		zPath += sunToOuterSpeed;
	}

}

void LookAtFromOuterToSun()
{
	static GLfloat zPath = 15.0f;
	gluLookAt(0.0f, 0.0, zPath, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); //bottom

	if (zPath <= 0.0)
	{
		isYZRotationDone = true;
	}
	else
	{
		zPath -= outerToSunSpeed;
	}
}
void LookAtYZRotation()
{
	glLoadIdentity();
	GLfloat distance = 15.0f;

	static GLfloat i = 0.0f;
	GLfloat zCordinate = (GLfloat) distance * cos((M_PI * i) / 180);
	GLfloat yCordinate = (GLfloat) distance * sin((M_PI * i) / 180);



	//fprintf(gpFILE, "i = %f, y = %f, z = %f, upy = %f, upz = %f\n", i, yCordinate, zCordinate, zCordinate / 6.0f, yCordinate / 6.0f);
	if (i < 360)
		i = i + fXYRotationFactor;//0.06

	if (i <= 90)
		gluLookAt(0.0f, yCordinate, zCordinate, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); //bottom
	else if (i <= 180)
		gluLookAt(0.0f, yCordinate, zCordinate, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f); //bottom
	else if (i <= 270)
		gluLookAt(0.0f, yCordinate, zCordinate, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f); //bottom
	else if (i <= 360)
		gluLookAt(0.0f, yCordinate, zCordinate, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	else
	{
		gluLookAt(0.0f, 0.0f, distance, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		isYZRotationDone = true;
	}

}

